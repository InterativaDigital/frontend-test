# Teste para candidatos à vaga de desenvolvedor Front-end da Interativa Digital.

Crie um fork deste repositório para salvar e nos dar acesso ao seu trabalho

### Instruções
1. Crie um fork deste repositório para salvar e nos dar acesso ao seu trabalho;
2. Criar o HTML/CSS com Bootstrap 4;
3. Nós queremos avaliar o seu nível de conhecimento em design responsivo usando Wordpress, HTML, CSS e JS;
4. Ter uma boa orientação de commits.

#### O que é permitido:
- Uso de Task runners/build tools como Webpack, Gulp, Grunt e afins;
- Pré-processadores CSS: Sass, LESS, Stylus.

### Material 
Disponibilizamos o PSD de referência para a execução deste desafio. Você pode encontralo-lo no diretorio PSD e arquivos de imagens no diretório Material.
#### Fonte
font google montserrat:
https://fonts.google.com/specimen/Montserrat
####Cores da arte:
![#262625](https://placehold.it/15/262625/000000?text=+)`#262625`
![#3b3b3b](https://placehold.it/15/3b3b3b/000000?text=+)`#3b3b3b`
![#ff9c00](https://placehold.it/15/ff9c00/000000?text=+)`#ff9c00`
![#ff8400](https://placehold.it/15/ff8400/000000?text=+)`#ff8400`
![#e48304](https://placehold.it/15/e48304/000000?text=+)`#e48304`
![#ffffff](https://placehold.it/15/ffffff/000000?text=+)`#ffffff`

### O objetivo desse teste é avaliar:
- Organização;
- Semântica;
- Funcionamento e usabilidade;
- Nível de prática com o Wordpress;
- Uso das features das linguagens (HTML, CSS e JS);
- Performance do código;
- README.md bem documentado com procedimentos para executar o projeto.
